from collections import defaultdict

def areAnagram(arr1, arr2, n, m):
	if (n != m):
		return False

	count = defaultdict(int)

	for i in arr1:
		count[i] += 1

	for i in arr2:
		if (count[i] == 0):
			return False
		else:
			count[i] -= 1

	return True


arr1 = [32, 52, 89, 90, 21]
arr2 = [21, 32, 90, 52, 89]

n = len(arr1)
m = len(arr2)

if areAnagram(arr1, arr2, n, m):
	print("Yes")
else:
	print("No")
