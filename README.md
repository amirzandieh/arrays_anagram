## Arrays Anagram

The solution for solving this problem is using hash table.
We store all elements of arr1[] and their counts in a hash table. Then we traverse arr2[] and check if the count of every element in arr2[] matches with the count in arr1[].

- Time Complexity: O(n)
- Auxiliary Space: O(n)